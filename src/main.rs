use rand::prelude::*;

// raw concrete stuff
// already exists
#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Debug)]
struct Image {
    pixels: usize,
}

impl Image {
    fn new(pixels: usize) -> Self {
        Self {
            pixels,
        }
    }

    // Same struct but the contents are generated randomly
    fn random() -> Self {
        Self {
            pixels: random(),
        }
    }
}

impl Operators for Image {
    fn add(self, right_hand: Expression) -> Expression {
        Image::ident(self).add(right_hand)
    }
    fn mul(self, right_hand: Expression) -> Expression {
        Image::ident(self).mul(right_hand)
    }
    fn ident(img: Image) -> Expression {
        Expression::ident(img)
    }
}

// Tree-like structure
#[derive(PartialEq, Debug, Clone)]
struct Expression {
    operator: Operator,
    left_hand: Option<Box<Expression>>,
    right_hand: Option<Box<Expression>>,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug, Clone)]
enum Operator {
    Add,
    Mul,
    Ident(Image),
}

trait Operators {

    fn add(self, right_hand: Expression) -> Expression;
    fn mul(self, right_hand: Expression) -> Expression;
    fn ident(img: Image) -> Expression;
}

impl Operators for Expression {
    fn add(self, right_hand: Self) -> Self {
        Self {
            operator: Operator::Add,
            left_hand: Some(Box::new(self)),
            right_hand: Some(Box::new(right_hand)),
        }
    }
    fn mul(self, right_hand: Self) -> Self {
        Self {
            operator: Operator::Mul,
            left_hand: Some(Box::new(self)),
            right_hand: Some(Box::new(right_hand)),
        }
    }
    fn ident(img: Image) -> Self {
        Self {
            operator: Operator::Ident(img.clone()),
            left_hand: None,
            right_hand: None,
        }
    }
}

trait Tiled {
    fn read_tile(&self) -> usize;
}

impl Tiled for Image{
    // Reads a tile from an image
    fn read_tile(&self) -> usize {
        self.pixels
    }
}

impl Tiled for Expression{
    // Reads a tile from an image
    fn read_tile(&self) -> usize {
        match &self.operator {
            Operator::Ident(img) => img.read_tile(),
            Operator::Add => self.left_hand.as_ref().unwrap().read_tile() + self.right_hand.as_ref().unwrap().read_tile(),
            Operator::Mul => self.left_hand.as_ref().unwrap().read_tile() * self.right_hand.as_ref().unwrap().read_tile(),
        }
    }
}

struct RandomImage {}

impl Tiled for RandomImage {
    fn read_tile(&self) -> usize {
        random()
    }
}

fn main() {
    let img1 = Image::new(1);
    let img2 = Image::new(2);
    let img3 = Image::new(3);

    let result1 = img1.clone().add(Image::ident(img2.clone()));
    let result2 = Expression::ident(img3);
    let result3 = result1.clone().mul(result2.clone());

    let result4 = result1.add(result2);

    let result5 = Expression::ident(img1.clone()).add(Expression::ident(Image::random()));

    // assert_eq!(result3, Operation {
    //     operator: Operator::Add,
    //     left_hand: Some(Box::new(Operation::ident(img1))),
    //     right_hand: Some(Box::new(Operation::ident(img2))),
    // });

    // // recursively applies operations and returns computed pixels
    // //result.read_tile("algo");


    // dbg!(result3);
    println!("===============");

    // dbg!(result4);
    //


    // println!("(1 + 2) * 3 is {}", result3.read_tile()); // This should be 9
    // println!("(1 + 2) + 3 is {}", result4.read_tile()); // This should be 6
    //
    dbg!(result5.clone());
    dbg!(result5);

    // println!("Random image is {}", Image::random().read_tile());
}
