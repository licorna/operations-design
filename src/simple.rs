use core::fmt::Debug;

// Ok, the idea behind this one is that we have 2 structs with different
// body (attributes) that can be merged with others in a "chain". This is
// they can be operated together.

#[derive(Debug)]
struct Point {
    x: i16,
    y: i16,
}

struct Point3 {
    x: i16,
    y: i16,
    z: i16,
}

#[derive(Debug)]
enum Operator {
    Add,
    Evaluate(Box<dyn Evaluable>)
}

impl Debug for dyn Evaluable {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{}", self.evaluate())
    }
}

trait Evaluable {
    fn evaluate(&self) -> i16;
}

impl Evaluable for Point {
    fn evaluate(&self) -> i16 {
        self.x + self.y
    }
}

#[derive(Debug)]
struct Operation {
    op: Operator,
    left: Option<Box<Operation>>,
    right: Option<Box<Operation>>,
}

impl Operation {
    fn evaluate(&self) -> i16 {
        match &self.op {
            Operator::Evaluate(v) => v.evaluate(),
            Operator::Add => todo!(),
        }
    }
}

fn main() {
    let p = Point { x: 1, y: 2 };
    let op = Operation{
        op: Operator::Evaluate(Box::new(p)),
        left: None,
        right: None,
    };

    dbg!(op);
}
